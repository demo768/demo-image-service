
'use strict';

const http = require('http');

function defaultTask(cb) {
  try {
    const hostname = '127.0.0.1';
    const port = 3000;

    const server = http.createServer((req, res) => {
      res.statusCode = 200;
      res.setHeader('Content-Type', 'text/plain');
      res.end('Demo - Image Service 1.0.2');
    });

    server.listen(port, hostname, () => {
      console.log(`Server running at http://${hostname}:${port}/`);
    });
  }
  catch(err) {
    console.log('Could not start.', err);
  }
  cb();
}

exports.default = defaultTask;
